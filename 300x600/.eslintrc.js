module.exports = {
	'env' : {
		'browser' : true,
		'es2020' : true
	},
	'extends' : 'eslint:recommended',
	'parserOptions' : {
		'ecmaVersion' : 11
	},
	'rules' : {
		'indent' : [
			'error',
			'tab',
			{
				'SwitchCase' : 1
			}
		],
		'linebreak-style' : [
			'error',
			'unix'
		],
		'quotes' : [
			'error',
			'single'
		],
		'semi' : [
			'error',
			'always'
		],
		'space-before-blocks' : [
			'error',
			'always'
		],
		'space-before-function-paren' : [
			'error',
			'always'
		],
		'space-in-parens' : [
			'error',
			'always'
		],
		'object-curly-spacing' : [
			'error',
			'always'
		],
		'array-bracket-spacing' : [
			'error',
			'always'
		],
		'key-spacing' : [
			'error',
			{
				'beforeColon' : true
			}
		],
		'comma-spacing' : 'error',
		'space-infix-ops' : 'error',
		'no-undef' : 'off',
		// 'no-unused-vars' : 'off'
	}
};