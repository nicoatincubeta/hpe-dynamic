//
// ────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: D O U B L E C L I C K   B O I L E R P L A T E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────
//

document.addEventListener( 'DOMContentLoaded', function () {

	if ( Enabler.isInitialized() ) {
		enablerInitHandler();
	} else {
		Enabler.addEventListener( studio.events.StudioEvent.INIT, enablerInitHandler );
	}

	function enablerInitHandler () {

		if ( Enabler.isPageLoaded() ) {
			pageLoadedHandler();
		} else {
			Enabler.addEventListener( studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler );
		}
	}

	function pageLoadedHandler () {

		if ( Enabler.isVisible() ) {
			adVisibilityHandler();
		} else {
			Enabler.addEventListener( studio.events.StudioEvent.VISIBLE, adVisibilityHandler );
		}
	}

	function adVisibilityHandler () {

		Dynamic.init();
		DOM.init();
		Creative.init();
	}
} );

//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: D Y N A M I C : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
// Dynamic invocation code.
// Use "Dynamic.get().key" to access the value.
// ie: var foo = Dynamic.get().ID;

var Dynamic = ( function () {

	function init () {
        
		Enabler.setProfileId( 10612330 );
		var devDynamicContent = {};

		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE = [ {} ];
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0]._id = 0;
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].id = 1;
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_label = '1_HPE_Master_300x600__DE';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].default = true;
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].active = true;
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].start_date = {};
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].start_date.RawValue = '2021-01-01 00:00';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].start_date.UtcValue = 1609488000000;
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].end_date = {};
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].end_date.RawValue = '2021-12-31 23:59';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].end_date.UtcValue = 1641023940000;
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].landingpage = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].utm_source = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].utm_medium = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].utm_campaign = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].utm_term = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].utm_content = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_level_1 = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_level_2 = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_level_3 = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_level_4 = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_level_5 = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].reporting_level_6 = '';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].img_bg = {};
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].img_bg.Type = 'file';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].img_bg.Url = 'https://s0.2mdn.net/ads/richmedia/studio/38744576/38744576_20210315100613174_bg-placeholder.jpg';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_bg = '{xPos : center, yPos : top}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].img_partner_logo = {};
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].img_partner_logo.Type = 'file';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].img_partner_logo.Url = 'https://s0.2mdn.net/ads/richmedia/studio/38744576/38744576_20210315100617964_hpe_pri_grn_rev_rgb.png';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_partner_logo = '{width : 90px, xPos : 10px, yPos : 10px}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].txt_frame1 = 'This is frame 1.|Random text here.|This is a small text.';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_frame1 = '{fontSize : 18px, lineHeight : 24px, fontColor : #ffffff, yPos : 90px}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].txt_frame2 = 'This is frame 2.|Random text here.|Update this on feed.|<span class="divider"></span>|Last line.';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_frame2 = '{fontSize : 18px, lineHeight : 24px, fontColor : #ffffff, yPos : 90px}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].txt_frame3 = 'This is frame 3.|Random text here.|Update this on feed.';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_frame3 = '{fontSize : 18px, lineHeight : 24px, fontColor : #ffffff, yPos : 90px}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].txt_frame4 = 'This is frame 4.|<span class="divider"></span>|Random text here.|<span class="divider"></span>|Final line here.';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_frame4 = '{fontSize : 18px, lineHeight : 24px, fontColor : #ffffff, yPos : 50px}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].txt_cta = 'JETZT ENTDECKEN';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].set_cta = '{fontSize : 12px, fontColor : #ffffff, backgroundColor : #ff0000, gradPos : 0deg, gradColor1 : #FF9400, gradColor2 : #EBC23B, borderRadius : 4px, width : 100px, offsetTop : -60px}';
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].exit_url = {};
		devDynamicContent.HPE_DynamicMasterset2021_Feed_DE[0].exit_url.Url = 'https://www.google.com'; 
		Enabler.setDevDynamicContent( devDynamicContent );
	}

	function get () {
        
		return dynamicContent.HPE_DynamicMasterset2021_Feed_DE[0];
	}

	return {
		init : init,
		get : get
	};
}() );

//
// ──────────────────────────────────────────────────────────────── I ──────────
//   :::::: C R E A T I V E   D O M : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────
// Setup your creative's DOM access here.
// Use "DOM.get().selectorName" to access the value.
// ie: DOM.get().wrapper;

var DOM = ( function () {

	var el = {};

	function init () {

		el.wrapper = Utilities.selector( '.main-wrapper' );
		el.bg = Utilities.selector( '.bg' );
		el.frame1Copy = Utilities.selector( '.frame-1-copy' );
		el.frame2Copy = Utilities.selector( '.frame-2-copy' );
		el.frame3Copy = Utilities.selector( '.frame-3-copy' );
		el.frame4Copy = Utilities.selector( '.frame-4-copy' );
		el.partnerLogo = Utilities.selector( '.partner-logo img' );
		el.cta = Utilities.selector( '.cta' );
		el.bgHotspot = Utilities.selector( '.bg-hotspot' );
	}

	function get () {

		return el;
	}

	return {
		init : init,
		get : get
	};

}() );

//
// ──────────────────────────────────────────────────────── I ──────────
//   :::::: C R E A T I V E : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────
// This is the main entry point of your creative.
// Creative logic, animation, events, and everything to make your creative work.

var Creative = ( function () {

	function init () {

		setup();
		animate();
		addEvents();
	}

	//
	// ───────────────────────────────────────────────────── SETUP DYNAMIC VALUES ─────
	// This is where you assign the dynamic values to your DOM elements.
    
	function setup () {
        
		/** Background Image */
		var backgroundImageConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_bg ) );
		DOM.get().bg.style.backgroundPosition = backgroundImageConfig.xPos + ' ' + backgroundImageConfig.yPos;
		DOM.get().bg.style.backgroundImage = 'url(' + Dynamic.get().img_bg.Url + ')';

		/** Partner Logo */
		var partnerLogoConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_partner_logo ) );
		DOM.get().partnerLogo.style.width = partnerLogoConfig.width;
		DOM.get().partnerLogo.style.left = partnerLogoConfig.xPos;
		DOM.get().partnerLogo.style.top = partnerLogoConfig.yPos;
		DOM.get().partnerLogo.setAttribute( 'src', Dynamic.get().img_partner_logo.Url );

		/** Frame 1 Copy */
		var frame1CopyConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_frame1 ) );
		DOM.get().frame1Copy.style.fontSize = frame1CopyConfig.fontSize;
		DOM.get().frame1Copy.style.lineHeight = frame1CopyConfig.lineHeight;
		DOM.get().frame1Copy.style.color = frame1CopyConfig.fontColor;
		DOM.get().frame1Copy.style.top = frame1CopyConfig.yPos;
		DOM.get().frame1Copy.innerHTML = Dynamic.get().txt_frame1.split( '|' ).join( '<br>' );

		/** Frame 2 Copy */
		var frame2CopyConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_frame2 ) );
		DOM.get().frame2Copy.style.fontSize = frame2CopyConfig.fontSize;
		DOM.get().frame2Copy.style.lineHeight = frame2CopyConfig.lineHeight;
		DOM.get().frame2Copy.style.color = frame2CopyConfig.fontColor;
		DOM.get().frame2Copy.style.top = frame2CopyConfig.yPos;
		DOM.get().frame2Copy.innerHTML = Dynamic.get().txt_frame2.split( '|' ).join( '<br>' );

		/** Frame 3 Copy */
		var frame3CopyConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_frame3 ) );
		DOM.get().frame3Copy.style.fontSize = frame3CopyConfig.fontSize;
		DOM.get().frame3Copy.style.lineHeight = frame3CopyConfig.lineHeight;
		DOM.get().frame3Copy.style.color = frame3CopyConfig.fontColor;
		DOM.get().frame3Copy.style.top = frame3CopyConfig.yPos;
		DOM.get().frame3Copy.innerHTML = Dynamic.get().txt_frame3.split( '|' ).join( '<br>' );

		/** Frame 4 Copy */
		var frame4CopyConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_frame4 ) );
		DOM.get().frame4Copy.style.fontSize = frame4CopyConfig.fontSize;
		DOM.get().frame4Copy.style.lineHeight = frame4CopyConfig.lineHeight;
		DOM.get().frame4Copy.style.color = frame4CopyConfig.fontColor;
		DOM.get().frame4Copy.style.top = frame4CopyConfig.yPos;
		DOM.get().frame4Copy.innerHTML = Dynamic.get().txt_frame4.split( '|' ).join( '<br>' );

		/** CTA */
		var ctaConfig = JSON.parse( Utilities.normalizeJSON( Dynamic.get().set_cta ) );
		console.log( ctaConfig );
		DOM.get().cta.style.fontSize = ctaConfig.fontSize;
		DOM.get().cta.style.color = ctaConfig.fontColor;
		DOM.get().cta.style.backgroundColor = ctaConfig.backgroundColor;
		DOM.get().cta.style.backgroundImage = 'linear-gradient(' + ctaConfig.gradPos + ',' + ctaConfig.gradColor1 + ',' + ctaConfig.gradColor2 + ')';
		DOM.get().cta.style.borderRadius = ctaConfig.borderRadius;
		DOM.get().cta.style.width = ctaConfig.width;
		DOM.get().cta.style.lineHeight = parseInt( ctaConfig.fontSize ) + 2 + 'px';
		DOM.get().cta.innerHTML = Dynamic.get().txt_cta;
		DOM.get().cta.style.top = DOM.get().frame4Copy.offsetTop + DOM.get().frame4Copy.offsetHeight + 20 + parseInt( ctaConfig.offsetTop ) + 'px';
	}

	//
	// ──────────────────────────────────────────────────────────────── ANIMATION ─────
	// This is where you'll place all animation related code.

	function animate () {
            
		/** Animation start values. */
		TweenMax.set( DOM.get().bg, { autoAlpha : 0.6, scale : 1.2 } );

		var st1 = new SplitText( DOM.get().frame1Copy, { type : 'lines' } );
		TweenMax.set( st1.lines, { autoAlpha : 0, y : 10 } );

		var st2 = new SplitText( DOM.get().frame2Copy, { type : 'lines' } );
		TweenMax.set( st2.lines, { autoAlpha : 0, y : 10 } );

		var st3 = new SplitText( DOM.get().frame3Copy, { type : 'lines' } );
		TweenMax.set( st3.lines, { autoAlpha : 0, y : 10 } );

		var st4 = new SplitText( DOM.get().frame4Copy, { type : 'lines' } );
		TweenMax.set( st4.lines, { autoAlpha : 0, y : 10 } );
        
		TweenMax.set( DOM.get().cta, { autoAlpha : 0, y : 10 } );


		/** Main animation code. */
		var tl = new TimelineMax();

		/** BG */
		tl.to( DOM.get().bg, 1, { autoAlpha : 0.6, scale : 1, ease : Power2.easeInOut } );
		/** Frame 1 */
		tl.staggerTo( st1.lines, 0.7, { autoAlpha : 1, y : 0, ease : Power2.easeInOut }, 0.1 );
		tl.staggerTo( st1.lines, 0.7, { autoAlpha : 0, y : -10, ease : Power2.easeInOut, delay : 1.3 }, 0.1 );
		/** Frame 2 */
		tl.staggerTo( st2.lines, 0.7, { autoAlpha : 1, y : 0, ease : Power2.easeInOut }, 0.1 );
		tl.staggerTo( st2.lines, 0.7, { autoAlpha : 0, y : -10, ease : Power2.easeInOut, delay : 1.3 }, 0.1 );
		/** Frame 3 */
		tl.staggerTo( st3.lines, 0.7, { autoAlpha : 1, y : 0, ease : Power2.easeInOut }, 0.1 );
		tl.staggerTo( st3.lines, 0.7, { autoAlpha : 0, y : -10, ease : Power2.easeInOut, delay : 1.3 }, 0.1 );
		/** Frame 4 */
		tl.staggerTo( st4.lines, 0.7, { autoAlpha : 1, y : 0, ease : Power2.easeInOut }, 0.1 );
		/** CTA */
		tl.to( DOM.get().cta, 0.7, { autoAlpha : 1, y : 0, ease : Power2.easeInOut } );
		tl.to( DOM.get().cta, 0.7, { scale : 1.2, ease : Power2.easeInOut }, '+=0.5' );
		tl.to( DOM.get().cta, 0.7, { scale : 1, ease : Power2.easeInOut }, '-=0.3' );

	}

	//
	// ─────────────────────────────────────────────────────────────────── EVENTS ─────
	// This is where you'll place all event related codes (ie: Exits)

	function addEvents () {

		DOM.get().cta.addEventListener( 'click', function () {
            
			Enabler.exitOverride( 'CTA Exit', Dynamic.get().exit_url.Url );
		} );

		DOM.get().bgHotspot.addEventListener( 'click', function () {
            
			Enabler.exitOverride( 'BG Exit', Dynamic.get().exit_url.Url );
		} );
	}

	return {
        
		init : init
	};
}() );

//
// ────────────────────────────────────────────────────────── I ──────────
//   :::::: U T I L I T I E S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────
//

var Utilities = ( function () {

	//
	// ─── QUERY SELECTOR ─────────────────────────────────────────────────────────────
	//
 
	function selector ( query ) {
 
		var t = document.querySelectorAll( query );
		return ( t.length === 0 ) ? false : ( t.length === 1 ) ? t[0] : t;
 
	}
    
	//
	// ─── TRIGGER CUSTOM EVENT ───────────────────────────────────────────────────────
	//

	function triggerEvent ( element, eventName, data ) {
		if ( document.createEvent ) {
			var event = document.createEvent( 'CustomEvent' );
			event.initCustomEvent( eventName, true, true, data );
		} else {
			// eslint-disable-next-line no-redeclare
			var event = new CustomEvent( eventName, { detail : data } );
		}
    
		element.dispatchEvent( event );
	}

	//
	// ─── NORMALIZE JSON ─────────────────────────────────────────────────────────────
	//

	function normalizeJSON ( str ) {

		return str.replace( /[\s\n\r\t]/gs, '' ).replace( /,([}\]])/gs, '$1' )
			.replace( /([,{\[]|)(?:("|'|)([\w_\- ]+)\2:|)("|'|)(.*?)\4([,}\]])/gs, ( str, start, q1, index, q2, item, end ) => {
				item = item.replace( /"/gsi, '' ).trim();
				if( index ) {index = '"' + index.replace( /"/gsi, '' ).trim() + '"';}
				if( !item.match( /^[0-9]+(\.[0-9]+|)$/ ) && ![ 'true', 'false' ].includes( item ) ) {item = '"' + item + '"';}
				if( index ) {return start + index + ':' + item + end;}
				return start + item + end;
			} );
	}
  
	return {
 
		selector : selector,
		triggerEvent : triggerEvent,
		normalizeJSON : normalizeJSON
	};
 
} )();